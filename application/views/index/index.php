<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0"><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/index'); ?>">Dashboard</a></li>
          <?php echo isset($condition) ? $condition : ''; ?>
          <!-- <li class="breadcrumb-item active">Starter Page</li> -->
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="section-body">
      <div class="card card-info">
        <div class="card-body">
          <h3>Selamat datang <?php echo $this->session->userdata('t_realName') ?></h3>
        </div>
        <button class="btn btn-success" id="btn-check" type="button"><i class="fas fa-check"></i></button>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content -->
<?php $this->load->view('template/template_scripts') ?>
<script>
  var site_url = '<?php echo site_url() ?>';
  $(function() {
    $('#btn-check').click(function(e) {
      e.preventDefault();
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: site_url + '/index/cekAPI',
        data: {
          nik: '3213231207800006'
        },
        dataType: 'json',
        success: function(response) {
          console.log(response);
        },
        error: function(xmlresponse) {
          console.log('errpr ' + xmlresponse);
        }
      })
    })
  })
</script>