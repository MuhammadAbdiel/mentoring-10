<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
    <meta http-equiv="refresh" content="1800;url=<?php echo site_url('index/logout') ?>" />
    <title><?php echo $this->config->item('webname') ?></title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/select2/select2.min.css') ?>">
    <!-- daterange -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/bootstrap-daterangepicker/daterangepicker.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/datepicker/css/datepicker.css') ?>">
    <!-- CSS Libraries -->
    <!-- datatables -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/bootstrap/datatables.bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/select-datatable.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('public/modules/sweetalert2-theme-bootstrap-4/bootstrap-4.css') ?>">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/chosen/bootstrap-chosen.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/chosen/chosen.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/summernote/summernote-lite.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/modules/summernote/summernote-bs4.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/loading_page.css'); ?>">
    <link href="<?php echo base_url('public/modules/bootstrap3-editable') ?>/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
    <link href="<?php echo base_url('public/modules/iziToast/dist/css/') ?>iziToast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/select2/select2.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/daterange/bootstrap-timepicker.min.css') ?>">

    <!-- Google Font: Source Sans Pro -->
    <link rel=" stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/adminLTE/plugins/fontawesome-free/css/all.min.css') ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('public/modules/adminLTE/dist/css/adminlte.min.css') ?>">

    <style>
        body {
            background-color: #F4F6F9;
        }
    </style>
</head>

<?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-user mr-2"></i>
                        <span>
                            Hi, <?php echo $this->session->userdata('t_username'); ?>
                        </span>
                        <i class="fas fa-chevron-down ml-3"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="<?php echo site_url('/index/logout'); ?>" class="dropdown-item text-center text-bold">
                            Logout
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4" style="position: fixed !important;">
            <!-- Brand Logo -->
            <a href="<?php echo site_url('index'); ?>" class="brand-link">
                <img src="<?php echo base_url('public/modules/adminLTE/dist/img/AdminLTELogo.png') ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Administrator</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="<?php echo site_url('index'); ?>" class="nav-link <?php echo is_active_page('index', 'active') ?>">
                                <i class="nav-icon fa fa-home"></i>
                                <p>
                                    Beranda
                                </p>
                            </a>
                        </li>
                        <?php if (in_array("akun.access", $userMenus)) : ?>
                            <li class="nav-item">
                                <a href="<?php echo site_url('akun/index') ?>" class="nav-link <?php echo is_active_page('akun', 'active') ?>">
                                    <i class="nav-icon fa fa-th-list"></i>
                                    <p>
                                        Akun
                                    </p>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (in_array("kategori.access", $userMenus)) : ?>
                            <li class="nav-item">
                                <a href="<?php echo site_url('kategori/index') ?>" class="nav-link <?php echo is_active_page('kategori', 'active') ?>">
                                    <i class="nav-icon fas fa-layer-group"></i>
                                    <p>
                                        Kategori
                                    </p>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (in_array("produk.access", $userMenus)) : ?>
                            <li class="nav-item">
                                <a href="<?php echo site_url('produk/index') ?>" class="nav-link <?php echo is_active_page('produk', 'active') ?>">
                                    <i class="nav-icon fa fa-shopping-cart"></i>
                                    <p>
                                        Produk
                                    </p>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (in_array("lembaga.access", $userMenus)) : ?>
                            <li class="nav-item">
                                <a href="<?php echo site_url('lembaga/index') ?>" class="nav-link <?php echo is_active_page('lembaga', 'active') ?>">
                                    <i class="nav-icon fa fa-image"></i>
                                    <p>
                                        Lembaga
                                    </p>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (in_array("devisi.access", $userMenus)) : ?>
                            <li class="nav-item">
                                <a href="<?php echo site_url('devisi/index') ?>" class="nav-link <?php echo is_active_page('devisi', 'active') ?>">
                                    <i class="nav-icon fa fa-tags"></i>
                                    <p>
                                        Devisi
                                    </p>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item <?php echo is_active_page('kategori_artikel', 'menu-open');
                                            echo is_active_page('artikel', 'menu-open');
                                            echo is_active_page('slider', 'menu-open'); ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-newspaper"></i>
                                <p>
                                    Artikel
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo site_url('kategori_artikel/index'); ?>" class="nav-link <?php echo is_active_page('kategori_artikel', 'active') ?>">
                                        <i class="far fa-newspaper nav-icon"></i>
                                        <p>Kategori Artikel</p>
                                    </a>
                                </li>
                                <?php if (in_array("artikel.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('artikel/index'); ?>" class="nav-link <?php echo is_active_page('artikel', 'active') ?>">
                                            <i class="far fa-newspaper nav-icon"></i>
                                            <p>Blog Artikel</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (in_array("slider.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('slider/index'); ?>" class="nav-link <?php echo is_active_page('slider', 'active') ?>">
                                            <i class="far fa-newspaper nav-icon"></i>
                                            <p>Slider Highlight</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>


                        <li class="nav-item <?php echo is_active_page('agen', 'menu-open');
                                            echo is_active_page('kantor_cabang', 'menu-open');
                                            echo is_active_page('pegawai', 'menu-open'); ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-th-list"></i>
                                <p>
                                    Data Pokok
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <?php if (in_array("agen.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('agen/index'); ?>" class="nav-link <?php echo is_active_page('agen', 'active') ?>">
                                            <i class="far fa-th-list nav-icon"></i>
                                            <p>Agen</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (in_array("pegawai.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('pegawai/index'); ?>" class="nav-link <?php echo is_active_page('pegawai', 'active') ?>">
                                            <i class="far fa-th-list nav-icon"></i>
                                            <p>Pegawai</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (in_array("kantor_cabang.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('kantor_cabang/index'); ?>" class="nav-link <?php echo is_active_page('kantor_cabang', 'active') ?>">
                                            <i class="far fa-th-list nav-icon"></i>
                                            <p>Kantor Cabang</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>


                        <li class="nav-item <?php echo is_active_page('jamaah', 'menu-open');
                                            echo is_active_page('jamaah_birthday', 'menu-open'); ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Jama'ah
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <?php if (in_array("jamaah.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('jamaah/index'); ?>" class="nav-link <?php echo is_active_page('jamaah', 'active') ?>">
                                            <i class="far fa-users nav-icon"></i>
                                            <p>Data Jamaah</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (in_array("jamaah_birthday.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('jamaah_birthday/index'); ?>" class="nav-link <?php echo is_active_page('jamaah_birthday', 'active') ?>">
                                            <i class="far fa-users nav-icon"></i>
                                            <p>Ulang Tahun Jamaah</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>



                        <li class="nav-item <?php echo is_active_page('tabrur', 'menu-open');
                                            echo is_active_page('tabah', 'menu-open'); ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-gem"></i>
                                <p>
                                    Tabungan
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo site_url('tabrur/index'); ?>" class="nav-link <?php echo is_active_page('tabrur', 'active') ?>">
                                        <i class="far fa-gem nav-icon"></i>
                                        <p>Tabrur</p>
                                    </a>
                                </li>
                                <?php if (in_array("tabah.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('tabah/index'); ?>" class="nav-link <?php echo is_active_page('tabah', 'active') ?>">
                                            <i class="far fa-gem nav-icon"></i>
                                            <p>Tabah</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>



                        <li class="nav-item <?php echo is_active_page('rekap', 'active') ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-calendar"></i>
                                <p>
                                    Rekap
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo site_url('rekap/index/kota') ?>" class="nav-link <?php echo (strpos(uri_string(), 'kota') !== false ? 'active' : '') ?>">
                                        <i class="far fa-calendar nav-icon"></i>
                                        <p>Berdasarkan Kota</p>
                                    </a>
                                </li>
                                <?php if (in_array("rekap.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('rekap/index/marketing') ?>" class="nav-link <?php echo (strpos(uri_string(), 'marketing') !== false ? 'active' : '') ?>">
                                            <i class="far fa-calendar nav-icon"></i>
                                            <p>Berdasarkan Marketing</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (in_array("rekap.access", $userMenus)) : ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url('rekap/index/seat') ?>" class="nav-link <?php echo (strpos(uri_string(), 'seat') !== false ? 'active' : '') ?>">
                                            <i class="far fa-calendar nav-icon"></i>
                                            <p>Berdasarkan Seat</p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>

                        <hr>

                        <li class="nav-item">
                            <a href="<?php echo site_url('index/logout'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    Log Out
                                </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            {CONTENT}
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
</body>

</html>