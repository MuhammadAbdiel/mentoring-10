<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login &mdash; <?php echo $this->config->item('webname') ?></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('public/modules/css/bootstrap.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/css/all.css')?>">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo base_url()?>public/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>public/css/components.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/loading_page.css'); ?>">
  <!-- Template CSS -->
  <script type="text/javascript" src="<?php echo base_url('public/modules/js/google.js')?>"></script>
  <!-- font awesome -->
  <script src="<?php echo base_url('public/modules/js/jquery-3.3.1.min.js'); ?>"></script>
</head>
<body>
      {CONTENT}

  <!-- General JS Scripts -->
  <script src="<?php echo base_url('public/modules/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/modules/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/modules/js/tooltip.js'); ?>"></script>
  <script src="<?php echo base_url('public/modules/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/modules/js/jquery.nicescroll.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/modules/js/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/js/stisla.js')?>"></script>
  <script src="<?php echo base_url()?>public/js/scripts.js"></script>
  <script src="<?php echo base_url()?>public/js/custom.js"></script>
  <!-- JS Libraies -->
  <!-- Template JS File -->
  <script src="<?php echo base_url('public/modules/chosen/chosen.jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url()?>public/responsivevoice.js"></script>

  <!-- Page Specific JS File -->
  <script type="text/javascript">
    function show () {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");
    }
    // function hide () {
    //   document.getElementById("spinner-front").classList.remove("show");
    //   document.getElementById("spinner-back").classList.remove("show");
    // }
  function loading() {
    show();
  }
  function suara(kata){
   responsiveVoice.speak(
    kata,
    "Indonesian Female",
    {
     pitch: 1,
     rate: 1,
     volume: 1
    }
   );
  }
  <?php
      if(!empty($this->session->flashdata('gagal'))){

        ?>
        suara('<?php echo $this->session->flashdata('kata_gagal'); ?>');
        $('#not_login').html('<?php echo $this->session->flashdata('gagal')?>').fadeIn();
  <?php
      }
  ?>
  </script>
</body>
</html>
