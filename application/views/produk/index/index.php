<?php $this->load->view('template/template_scripts') ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0"><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/index'); ?>">Dashboard</a></li>
          <?php echo isset($condition) ? $condition : ''; ?>
          <!-- <li class="breadcrumb-item active">Starter Page</li> -->
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="section-body">
      <div class="card card-info">
        <div class="card-body">
          <?php echo $output->output; ?>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content -->

<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}

?>