<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{
    protected $template = "app";
    protected $module = "products";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Products';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Products</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('products');

        $crud->columns(['name', 'stock', 'is_active']);

        $crud->fields(['name', 'stock', 'is_active']);
        $crud->requiredFields(['name', 'stock']);
        $crud->defaultOrdering('name', 'asc');

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['created_at'] = date('Y-m-d');
            $stateParameters->data['slug'] = url_title(strtolower($stateParameters->data['name']), '-', true);
            return $stateParameters;
        });

        $crud->displayAs('name', 'Nama Produk');
        $crud->displayAs('stock', 'Stock Produk');
        $crud->displayAs('is_active', 'Status Produk');

        $crud->callbackColumn('is_active', function ($value, $row) {
            if ($value == 1) {
                return '<span class="badge badge-success">Active</span>';
            } else {
                return '<span class="badge badge-danger">Inactive</span>';
            }
        });

        $crud->fieldType('is_active', 'dropdown', ['1' => 'Active', '0' => 'Inactive']);

        // $crud->callbackAddField('is_active', function () {
        //     return '<select name="is_active" class="form-control">
        //     <option value="1">Active</option>
        //     <option value="0">Inactive</option>
        //     </select>';
        // });

        // $crud->callbackEditField('is_active', function ($value, $primary_key) {
        //     $data = $this->db->get_where('products', ['id' => $primary_key])->row();
        //     if ($data->is_active == 1) {
        //         $active = 'selected';
        //         $inactive = '';
        //     } else {
        //         $active = '';
        //         $inactive = 'selected';
        //     }
        //     return '<select name="is_active" class="form-control">
        //     <option value="1" ' . $active . '>Active</option>
        //     <option value="0" ' . $inactive . '>Inactive</option>
        //     </select>';
        // });


        if ($this->cek_hak_akses('access')) {
            $crud->unsetJquery();
            $output = $crud->render();

            $this->_setOutput($output);
        } else {
            redirect('admin/Index');
        }
    }

    function _setOutput($output = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('products/index/index', $x);
        $this->layout->publish();
    }
}
