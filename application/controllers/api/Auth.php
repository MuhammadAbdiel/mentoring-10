<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller {

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $error = '500';

    function __construct() {
        parent::__construct();
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_login', 'login');
    }

    public function index_post() {
        // check on posted data
        $data = json_decode(trim(file_get_contents('php://input')), true);

        // init result
        $result = null;

        // check wether is login, or refresh token
        if ($data != null && array_key_exists('username', $data) && array_key_exists('password', $data)) { // general login
            $username = $data['username'];
            $password = $data['password'];

            $result = $this->login->login($username, $password);
        } else if ($data != null && array_key_exists('refresh_token', $data)) { // refresh token
            $refresh = $data['refresh_token'];

            $check_token = AUTHORIZATION::validateToken($refresh);
            if ($check_token == false) {
                $this->response([
                    'status' => $this->error,
                    'error' => 'Internal Server Error'
                        ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                return;
            }

            // get username based on token
            $username = $check_token->username;

            $result = $this->login->check_user($username);
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Bad Request'
                    ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (is_array($result) && $result != null) {
            if ($result['status'] == 'ok') {
                // create jwt token
                $token = $this->_create_token($result['data'], $username);

                $this->response([
                    'status' => $this->ok,
                    'data' => $token
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => $result['message']
                        ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->error,
                'error' => 'Internal Server Error'
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get_index() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST::HTTP_BAD_REQUEST);
    }

    private function _create_token($data, $username) {
        $token = array();
        $token['id_user'] = $data['id_user'];
        $token['id_group'] = $data['id_group'];

        $refresh_token = array();
        $refresh_token['username'] = $username;

        try {
            $jwt = AUTHORIZATION::generateToken($token);
            $refresh = AUTHORIZATION::generateToken($refresh_token);
        } catch (Exception $error) {
            $jwt = null;
            $refresh = null;
        }

        $CI =& get_instance();
        $result = array();
        $result['token'] = $jwt;
        $result['expires'] = ($CI->config->item('token_timeout') > 0) ? ($CI->config->item('token_timeout') * 60) : 'never';
        $result['refresh_token'] = $refresh;

        return $result;
    }
}
