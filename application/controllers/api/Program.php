<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Program extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_program', 'program');
    }

    public function program_get() {

        $get = $this->program->getProgram();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function programLimit_get() {

        $get = $this->program->getProgram(8);
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function hari_get() {

        $get = $this->program->getHari();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function detail_post() {
        $data =json_decode(trim(file_get_contents('php://input')), true);
        if (!empty($data)) {
            $get = $this->program->getDetail($data['id_kelas_program']);
            if (is_array($get) && $get != null) {
                if ($get['status'] == 'ok') {
                    $result = $get['data'];

                    $this->response([
                        'status' => $this->ok,
                        'data' => $result
                            ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => $get['message']
                            ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => 'Data tidak ditemukan'
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        }else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
