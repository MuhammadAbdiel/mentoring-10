<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Login extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct() {

        parent::__construct();
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_auth', 'auth');
    }

    public function index_post() {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        if ($data != null && array_key_exists("username", $data) && array_key_exists("password", $data)) {

            $result = $this->auth->login($data['username'], $data['password']);
            $username = $data['username'];
            $password = $data['password'];

        }else if ($data!= null && array_key_exists("refresh_token", $data)){

            $refresh = $data['refresh_token'];

            $check_token = AUTHORIZATION::validateToken($refresh);

            if ($check_token==false) {

                $this->response([
                    'status'=>$this->error,
                    'message'=>"Internal Server Error",
                    "data"=>""], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

            $username = $check_token->username;
            $result = $this->login->check_user($username);

        }else {
            $this->response([
                'status'=>$this->bad,
                'message' =>'Bad request',
                'data' =>'0'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }

        if (is_array($result) && $result!= null) {

            if ($result['status']!='failed') {

                $token = $this->_create_token($result['data'], $username);

                $result['data']['token'] = $token;


                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {

                $this->response([
                    'status'=>$this->unauthorized,
                    'message'=>'unauthorized',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        } else {

            $this->response([
                'status'=>$this->error,
                'message'=>'Internal Server error',
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _create_token($data, $username) {
        $token = array();
        $token['id_user'] = $data['id_user'];
        $token['id_group'] = $data['id_group'];

        $refresh_token = array();
        $refresh_token['username'] = $username;

        try {
            $jwt = AUTHORIZATION::generateToken($token);
            $refresh = AUTHORIZATION::generateToken($refresh_token);
        } catch (Exception $error) {
            $jwt = null;
            $refresh = null;
        }

        $CI =& get_instance();
        $result = array();
        $result['token'] = $jwt;
        $result['expires'] = ($CI->config->item('token_timeout') > 0) ? ($CI->config->item('token_timeout') * 60) : 'never';
        $result['refresh_token'] = $refresh;

        return $result;
    }

    public function detail_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $result = $this->artikel->detail_artikel($data['id_artikel']);
        if (is_array($result) && $result != null) {
            if ($result['status']=='ok') {

                $this->response(['status'=>$this->ok,
                                'message'=>$result['message'],
                                'data'=>$result['data']],
                                REST_Controller::HTTP_OK);
            }else {

                $this->response(['status'=>$this->notfound,
                                'message'=>$result['message'],
                                'data'=>'0'],
                                REST_Controller::HTTP_NOT_FOUND);
            }
        }else {

            $this->response(['status'=>$this->notfound,
                            'message'=>$result['message'],
                            'data'=>'0'],
                            REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
