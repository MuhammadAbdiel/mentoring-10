<?php defined('BASEPATH')OR exit('no access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;

 class Berkas extends REST_Controller
 {
     function uploadBerkas_post()
     {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        echo '<pre>';
        print_r($this->input->post('judul_artikel'));
        exit();
        // Front End Path
        // $front_url = $this->config->item('front_url');
        $front_url = base_url();
        $front_url = substr($front_url, -1) == DIRECTORY_SEPARATOR ? $front_url : $front_url . DIRECTORY_SEPARATOR;

        try {
            date_default_timezone_set('Asia/Jakarta');

            $raw_data = $_FILES['file']['name'];

            $url = rtrim(base_url(), "/");
            $url2 = explode('/', $url);
            $folder = end($url2);
            $directory = $this->direktori($folder);

            $raw_location = $directory['fpath'];
            $dynamic_location = $directory['dpath'];


            $ori_name = $raw_data;
            $ex = explode(".", $ori_name);
            $extension = end($ex);

            $file_name = underscore("dicoba_" . date("YmdHis") . "." . $extension);

            $uploadfile = $raw_location. $dynamic_location . $file_name;
            $webpath = $dynamic_location . $file_name;

            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                // Delete Image First
                // if ($exist_url != null && $exist_url != "") {
                //     // delete it..
                //     $dir_upload = $this->config->item('dir_upload');
                //     $dir_upload = substr($dir_upload, -1) == DIRECTORY_SEPARATOR ? $dir_upload : $dir_upload . DIRECTORY_SEPARATOR;
                //     $exist_url = str_replace($front_url, "", $exist_url);
                //     $file_exist = $dir_upload . $exist_url;

                //     if (file_exists($file_exist)) {
                //         unlink($file_exist);
                //     }
                // }
                // // end delete file

                // // Set to DB
                // $this->db->trans_begin();
                // $this->db->query("
                //     UPDATE peserta_legalitas SET url_photo = ?
                //     WHERE id_legalitas = ?
                // ", array($front_url . $webpath, $id));
                // // End set to DB

                // if ($this->db->trans_status() === FALSE) {
                //     $this->db->trans_rollback();
                //     return ["status" => "failed", "message" => "Gagal Upload Data. Silahkan hubungi Administrator."];
                // } else {
                //     $this->db->trans_commit();
                //     $result = array('path' => $front_url . $webpath);

                //     return ["status" => "ok", "data" => $result];
                // }
                    return ["status" => "ok", "data" => 'file diupload'];
            } else {
                return ["status" => "failed", "message" => "Failed upload file."];
            }
        } catch (Exception $e) {
            return ["status" => "failed", "message" => $e];
        }
     }

     function uploadCI_post()
     {

        if (isset($_FILES['foto_artikel']['name'])) {
            $config['upload_path'] = 'files/artikel/'; //path folder file upload
            $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
            $config['max_size'] = '2000';
            $config['file_name'] = "uploadAPI_" . date('ymdhis'); //enkripsi file name upload
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('foto_artikel')) {
                $file_foto = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './files/artikel/' . $file_foto['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '50%';
                $config['new_image'] = './files/artikel/' . $file_foto['file_name'];
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $nama_foto = 'files/artikel/' . $file_foto['file_name'];
                $artikel['foto_artikel'] = $nama_foto;
                echo 'upload berhasil';
                print_r($this->input->post('judul_artikel'));
            }else {
                echo "belum diupload";
            }
        }else {
            echo 'belum diset';
        }
     }

     function direktori($folder){
        $full_path = $this->config->item('dir_upload');
        $full_path = substr($full_path, -1) == DIRECTORY_SEPARATOR ? $full_path : $full_path . DIRECTORY_SEPARATOR;
        $dynamic_path = 'files' . DIRECTORY_SEPARATOR . 'dokumen' . DIRECTORY_SEPARATOR ;

        // $location = $full_path . $dynamic_path;
        $location = $full_path;

		if (!file_exists($location)) {
            mkdir($location, 0777, true);
        }

		return array("fpath" => $location, "dpath" => $dynamic_path);
    }

    public function filter($data)
    {   $data = preg_replace('/[^a-zA-Z0-9]/', '', $data);
        return $data;
        unset($data);
    }
 }
  ?>