<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Keberangkatan extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_keberangkatan', 'keberangkatan');
    }

    public function keberangkatan_get() {

        $get = $this->keberangkatan->keberangkatan_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function search_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $result = $this->keberangkatan->keberangkatan_search($data['keyword']);
        if (is_array($result) && $result!=null) {
            if ($result['status']=='ok') {
                $response = $result['data'];
                $this->response(['status'=>$this->ok,'message'=>$result['message'],'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response(['status'=>$this->notfound,'message'=>$result['message'],'data'=>'0'], REST_Controller::HTTP_NOT_FOUND);
            }
        }else {
            $this->response(['status'=>$this->notfound,'message'=>$result['message'],'data'=>'0'], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function detail_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $result = $this->keberangkatan->get_detail($data['id_group']);
        if (is_array($result) && $result != null) {
            if ($result['status']=='ok') {

                $this->response(['status'=>$this->ok,
                                'message'=>$result['message'],
                                'data'=>$result['data']],
                                REST_Controller::HTTP_OK);
            }else {

                $this->response(['status'=>$this->notfound,
                                'message'=>$result['message'],
                                'data'=>'0'],
                                REST_Controller::HTTP_NOT_FOUND);
            }
        }else {

            $this->response(['status'=>$this->notfound,
                            'message'=>$result['message'],
                            'data'=>'0'],
                            REST_Controller::HTTP_NOT_FOUND);
        }
    }

     // ini contoh
    public function get_province() {
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->provinces_data();
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    //ini contoh
    public function get_regency() {
        $headers = $this->input->request_headers();
        $query_url = $this->input->get();

        // query parameter
        $province_id = null; // if null, get all regencies
        if ($query_url != false && array_key_exists('province_id', $query_url)) {
            $province_id = $query_url['province_id'];
        }

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->regencies_data($province_id);
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    //ini contoh
    public function get_district() {
        $headers = $this->input->request_headers();
        $query_url = $this->input->get();

        // query parameter
        $regency_id = null; // if null, get all regencies
        if ($query_url != false && array_key_exists('regency_id', $query_url)) {
            $regency_id = $query_url['regency_id'];
        }

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);
            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group')) {
                $get = $this->master->districts_data($regency_id);
                if (is_array($get) && $get != null) {
                    if ($get['status'] == 'ok') {
                        $result = $get['data'];

                        $this->response([
                            'status' => $this->ok,
                            'data' => $result
                                ], REST::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => $this->notfound,
                            'data' => $get['message']
                                ], REST::HTTP_NOT_FOUND);
                    }
                } else {
                    $this->response([
                        'status' => $this->notfound,
                        'data' => 'Data tidak ditemukan'
                            ], REST::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status' => $this->unauthorized,
                    'error' => 'Unathorized/Invalid Token'
                        ], REST::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status' => $this->bad,
                'error' => 'Token tidak ditemukan.'
                    ], REST::HTTP_BAD_REQUEST);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
