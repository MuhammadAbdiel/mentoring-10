<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Artikel extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_artikel', 'artikel');
    }

    public function artikel_post() {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        $get = $this->artikel->data_artikel($data['limit']);
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function detail_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $result = $this->artikel->detail_artikel($data['id_artikel']);
        if (is_array($result) && $result != null) {
            if ($result['status']=='ok') {

                $this->response(['status'=>$this->ok,
                                'message'=>$result['message'],
                                'data'=>$result['data']],
                                REST_Controller::HTTP_OK);
            }else {

                $this->response(['status'=>$this->notfound,
                                'message'=>$result['message'],
                                'data'=>'0'],
                                REST_Controller::HTTP_NOT_FOUND);
            }
        }else {

            $this->response(['status'=>$this->notfound,
                            'message'=>$result['message'],
                            'data'=>'0'],
                            REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
