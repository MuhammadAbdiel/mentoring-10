<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{
    protected $template = "app";
    protected $module = "produk";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Produk';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Produk</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('produk');
        $crud->setRelation('id_kategori', 'kategori', 'nama_kategori');

        $crud->columns(['nama_produk', 'id_kategori', 'harga_produk', 'stock_produk', 'warna_produk', 'is_active']);
        $crud->setRead();

        $crud->fields(['nama_produk', 'id_kategori', 'harga_produk', 'stock_produk', 'warna_produk', 'is_active']);
        $crud->requiredFields(['nama_produk', 'id_kategori', 'harga_produk', 'stock_produk', 'warna_produk', 'is_active']);
        $crud->defaultOrdering('nama_produk', 'asc');

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['id_produk'] = getUUID();
            $stateParameters->data['slug_produk'] = url_title(strtolower($stateParameters->data['nama_produk']), '-', true);
            $stateParameters->data['created_at'] = date('Y-m-d');
            $stateParameters->data['updated_at'] = date('Y-m-d');
            return $stateParameters;
        });

        $crud->callbackBeforeUpdate(function ($stateParameters) {
            $stateParameters->data['slug_produk'] = url_title(strtolower($stateParameters->data['nama_produk']), '-', true);
            $stateParameters->data['updated_at'] = date('Y-m-d');
            return $stateParameters;
        });

        $crud->displayAs('id_produk', 'Lihat Detail');
        $crud->displayAs('nama_produk', 'Nama Produk');
        $crud->displayAs('id_kategori', 'Kategori');
        $crud->displayAs('harga_produk', 'Harga Produk');
        $crud->displayAs('stock_produk', 'Stock Produk');
        $crud->displayAs('warna_produk', 'Warna Produk');
        $crud->displayAs('is_active', 'Status Produk');

        $crud->callbackColumn('is_active', function ($value, $row) {
            if ($value == 1) {
                return '<span class="badge badge-success">Aktif</span>';
            } else {
                return '<span class="badge badge-danger">Tidak Aktif</span>';
            }
        });

        $crud->fieldType('is_active', 'dropdown', ['1' => 'Aktif', '0' => 'Tidak Aktif']);

        if ($this->cek_hak_akses('access')) {
            $crud->unsetJquery();
            $output = $crud->render();

            $output->js_files = array_merge($output->js_files, [
                base_url('public/custom/script.js')
            ]);

            $this->_setOutput($output);
        } else {
            redirect('admin/Index');
        }
    }

    function _setOutput($output = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('produk/index/index', $x);
        $this->layout->publish();
    }
}
