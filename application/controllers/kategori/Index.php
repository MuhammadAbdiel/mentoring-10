<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{
    protected $template = "app";
    protected $module = "kategori";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Kategori';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Kategori</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('kategori');

        $crud->columns(['nama_kategori']);

        $crud->fields(['nama_kategori']);
        $crud->requiredFields(['nama_kategori']);
        $crud->defaultOrdering('nama_kategori', 'asc');

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['id_kategori'] = getUUID();
            return $stateParameters;
        });

        $crud->displayAs('nama_kategori', 'Nama Kategori');

        if ($this->cek_hak_akses('access')) {
            $crud->unsetJquery();
            $output = $crud->render();

            $this->_setOutput($output);
        } else {
            redirect('admin/Index');
        }
    }

    function _setOutput($output = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('kategori/index/index', $x);
        $this->layout->publish();
    }
}
