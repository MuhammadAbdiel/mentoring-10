<?php defined('BASEPATH')OR exit('no access allowed');
/**
  * summary
  */
 class M_customer extends MY_Model
 {
     /**
      * summary
      */
    protected $_table_name = "customer";
    protected $_order_by ="id_customer";
    protected $_order_by_type ="ASC";
    protected $_primary_key = "id_customer";


     public function __construct()
     {
     	parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
     }

     public function DataTableJamaah($data)
     {
      $return = array('total'=>0,'rows'=>array());


      $this->db->start_cache();
      $this->db->select('*');
      if (!empty($data['sSearch']) || $data['sSearch'] !='') {
        $search = $this->db->escape_str($data['sSearch']);
        $this->db->where("(nama LIKE '%{$search}%') OR (nomor_hp LIKE '%{$search}%') OR (kode_customer LIKE '%{$search}%')");
      }

      if (!empty($data['id_provinsi']) || $data['id_provinsi'] !='') {
        $id_provinsi = $data['id_provinsi'];
        $this->db->where("id_provinsi", $id_provinsi);
      }
      if (!empty($data['kabupaten']) || $data['kabupaten'] !='') {
        $kabupaten = $data['kabupaten'];
        $this->db->where("id_kabupaten", $kabupaten);
      }

      if (!empty($data['kantor_administrasi']) || $data['kantor_administrasi'] !='') {
        $kantor_administrasi = $data['kantor_administrasi'];
        $this->db->where("id_cabang", $kantor_administrasi);
      }

      if ($this->session->userdata('t_username')!="admin@rosanatravel.com") {
        $this->db->where("created_by", $this->session->userdata('t_userId'));
      }

      $this->db->stop_cache();
      $rs = $this->db->count_all_results($this->_table_name);
      $return['total'] = $rs;
      if ($return['total'] >0) {
      $this->db->select('*');

        $this->db->limit($data['limit'], $data['start']);
        $rs = $this->db->get($this->_table_name);
        if($rs->num_rows())
          $return['rows'] = $rs->result_array();
      }
      $this->db->flush_cache();
      return $return;
     }

     public function DataTableJamaahUltah($data)
     {
      $return = array('total'=>0,'rows'=>array());


      $this->db->start_cache();
      $this->db->select('*');
      if (!empty($data['sSearch']) || $data['sSearch'] !='') {
        $search = $this->db->escape_str($data['sSearch']);
        $this->db->where("(nama LIKE '%{$search}%') OR (nomor_hp LIKE '%{$search}%')");
      }

      if (!empty($data['bulan']) || $data['bulan'] !='') {
        $bulan = $data['bulan'];
        $this->db->where("MONTH(tanggal_lahir)", $bulan);
      }

      $this->db->stop_cache();
      $rs = $this->db->count_all_results($this->_table_name);
      $return['total'] = $rs;
      if ($return['total'] >0) {
      $this->db->select('*');

        $this->db->limit($data['limit'], $data['start']);
        $rs = $this->db->get($this->_table_name);
        if($rs->num_rows())
          $return['rows'] = $rs->result_array();
      }
      $this->db->flush_cache();
      return $return;
     }

     public function DataTablereveral($data)
     {
      $return = array('total'=>0,'rows'=>array());


      $this->db->start_cache();
      $this->db->select('*');
      if (!empty($data['sSearch']) || $data['sSearch'] !='') {
        $search = $this->db->escape_str($data['sSearch']);
        $this->db->where("(nominal_komisi LIKE '%{$search}%')");
      }

      if (!empty($data['id_customer']) || $data['id_customer'] !='') {
        $id_customer = $data['id_customer'];
        $this->db->where("to_id_customer", $id_customer);
      }

      $this->db->stop_cache();
      $rs = $this->db->count_all_results('trx_referal');
      $return['total'] = $rs;
      if ($return['total'] >0) {
      $this->db->select('*');

      if (!empty($data['id_customer']) || $data['id_customer'] !='') {
        $id_customer = $data['id_customer'];
        $this->db->where("to_id_customer", $id_customer);
      }
        $this->db->limit($data['limit'], $data['start']);
        $rs = $this->db->get('trx_referal');
        if($rs->num_rows())
          $return['rows'] = $rs->result_array();
      }
      $this->db->flush_cache();
      return $return;
     }

     public function getKantorCabang()
     {
       $this->db->select("*");
       $this->db->from("cabang");
       $result = $this->db->get();
       return $result->result_array();
     }

     public function getKantor($id)
     {
       $this->db->select("nama_cabang");
       $this->db->from("cabang");
       $this->db->where("id_cabang", $id);
       $result = $this->db->get()->row_array();
       return $result['nama_cabang'];
     }

     public function getTabrur($id_customer)
     {
      $this->db->start_cache();
      $this->db->select('*');
      $this->db->from("tabura");
      $this->db->where("id_customer", $id_customer);
      $result = $this->db->get();
      $data['tabrur'] = $result->row_array();

      $query = "SELECT tba.tanggal_pembayaran, tba.nominal, tba.mekanisme, tba.catatan, 'setoran' as type FROM tabura_setoran_awal tba
              WHERE tba.id_tabura=?
              UNION
              SELECT tst.tanggal_pembayaran, tst.nominal, tst.mekanisme, tst.catatan, 'tabungan' as type FROM tabura_setoran_tabungan tst WHERE tst.id_tabura=?";

      $result2 = $this->db->query($query, array($data['tabrur']['id_tabura'],$data['tabrur']['id_tabura']));
      $data['transaksi'] = $result2->result_array();
      $this->db->stop_cache();
      $this->db->flush_cache();
      return $data;
     }


     public function getCustomer($id)
     {
       $this->db->select("*");
       $this->db->from("customer");
       $this->db->where("REPLACE(id_customer,'-','')", $id);
       $result = $this->db->get();
       return $result->row_array();
     }

     public function buatAkun($data)
     {
        $response['sucess'] = '';
        $response['message'] = '';

       $cek_email = $this->db->query("SELECT * FROM user where ((email IS NOT NULL AND email = ?))", array($data['email']));
       if ($cek_email->num_rows()>0) {
          $response['sucess'] = 'failed';
          $response['message'] = 'Email sudah terdaftar di aplikasi';
       }else {
         $cek_username = $this->db->query("SELECT * FROM user where ((kontak_name IS NOT NULL AND kontak_name = ?))", array($data['kontak_name']));
         if ($cek_username->num_rows()>0) {
           $response['sucess'] = 'failed';
            $response['message'] = 'Nomor HP sudah terdaftar di aplikasi';
         }else {
           $cek_kontak = $this->db->query("SELECT * FROM user where ((username IS NOT NULL AND username = ?))", array($data['kontak_name']));
           if ($cek_kontak->num_rows()>0) {
             $response['sucess'] = 'failed';
              $response['message'] = 'Nomor HP sudah terdaftar di aplikasi';
           }else {
             $this->db->trans_begin();

             $user['email'] = $data['email'];
             $user['username'] = $data['kontak_name'];
             $user['kontak_name'] = $data['kontak_name'];
             $user['real_name'] = $data['real_name'];
             $user['password'] = hash('sha256', $data['password']);
             $this->db->set($user);
             $this->db->insert('user', $user);
             $id_user = $this->db->insert_id();

             $this->db->insert('user_group_combo', array('id_user'=>$id_user,'id_group'=>'9'));

             $this->db->where("REPLACE(id_customer,'-','')", str_replace("-", "", $data['id_customer']));
             $this->db->update("customer", array("fk_id_user"=>$id_user,'email'=>$data['email'],'nomor_hp'=>$data['kontak_name']));

             if ($this->db->trans_status()===false) {
               $this->db->trans_rollback();
               $response['sucess'] = 'failed';
                $response['message'] = 'Data tidak bisa didaftarkan';
             }else {
               $this->db->trans_commit();
               $response['sucess'] = 'success';
                $response['message'] = 'Data akun customer berhasil ditambahkan';
             }
             $this->db->trans_complete();

           }

         }

       }

       return $response;

     }

     public function DataTableHistori($data)
     {
      $return = array('total'=>0,'rows'=>array());


      $this->db->start_cache();
      $this->db->select('*');
      if (!empty($data['sSearch']) || $data['sSearch'] !='') {
        $search = $this->db->escape_str($data['sSearch']);
        $this->db->where("(nama_paket LIKE '%{$search}%')");
      }

      if (!empty($data['id_customer']) || $data['id_customer'] !='') {
        $id_customer = $data['id_customer'];
        $this->db->where("id_customer", $id_customer);
      }

      $this->db->stop_cache();
      $rs = $this->db->count_all_results('customer_histories');
      $return['total'] = $rs;
      if ($return['total'] >0) {
      $this->db->select('*');

        $this->db->limit($data['limit'], $data['start']);
        $rs = $this->db->get("customer_histories");
        if($rs->num_rows())
          $return['rows'] = $rs->result_array();
      }
      $this->db->flush_cache();
      return $return;
     }

     public function hapusHistori($id)
     {
       $this->db->where("id_histori", $id);
       $this->db->delete("customer_histories");
       if ($this->db->affected_rows()) {
         return true;
       }else {
         return false;
       }
     }

     public function getPaketID($id)
     {
       $result = array();
       $paket = $this->db->query("SELECT id_kelas_program, nama_program from um_kelas_program where id_kelas_program=? order by created_at DESC", array($id));

       if ($paket->num_rows()>0) {
         $result = $paket->row_array();
       }else {
         $result = null;
       }
       return $result;
     }

     public function getMaskapaiID($id)
     {
       $maskapai = $this->db->query("SELECT id_maskapai, nama_maskapai from m_maskapai where id_maskapai=?", array($id));

       if ($maskapai->num_rows()>0) {
         $result = $maskapai->row_array();
       }else {
         $result = null;
       }
       return $result;
     }

     public function getMaster()
     {
      $result = array();
       $paket = $this->db->query("SELECT id_kelas_program, nama_program from um_kelas_program order by created_at DESC");

       if ($paket->num_rows()>0) {
         $result['paket'] = $paket->result_array();
       }else {
         $result['paket'] = array();
       }

       $hotel = $this->db->query("SELECT id_hotel, nama_hotel,jenis from m_hotel order by nama_hotel ASC");

       if ($hotel->num_rows()>0) {
         $result['hotel'] = $hotel->result_array();
       }else {
         $result['hotel'] = array();
       }

       $maskapai = $this->db->query("SELECT id_maskapai, nama_maskapai from m_maskapai order by nama_maskapai ASC");

       if ($maskapai->num_rows()>0) {
         $result['maskapai'] = $maskapai->result_array();
       }else {
         $result['maskapai'] = array();
       }

       return $result;
     }

     public function editHistori($id)
     {
       $this->db->select("*");
       $this->db->from("customer_histories");
       $this->db->where("id_histori", $id);
       $result = $this->db->get();
       return $result->row_array();
     }

     public function getHistoriCustomer($id)
     {
       $this->db->where("to_id_customer", $id);
       $result = $this->db->get('trx_referal');
       return $result->result_array();
     }

     public function getHotel($id_hotel)
     {
      $this->db->where("id_hotel", $id_hotel);
      $result = $this->db->get('m_hotel');
      return $result->row_array();
     }
 }
?>