<?php

class M_log_aktivitas extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function set_date() {
        $periode = $this->input->get("periode");

        if ($periode) {
            $tgl = urldecode($periode);

            $this->f_awal = date("Y-m-d", strtotime(substr($tgl, 0, 10)));
            $this->f_akhir = date("Y-m-d", strtotime(substr($tgl, 13, 23)));
        } else {
            $this->f_awal = date('Y-m-d', strtotime('-3 days'));
            $this->f_akhir = date('Y-m-d');
        }
        $awal = date("m/d/Y", strtotime($this->f_awal));
        $akhir = date('m/d/Y', strtotime($this->f_akhir));

        $array = array(
            'tgl_awal' => $this->f_awal,
            'tgl_akhir' => $this->f_akhir,
            'tglawal' => $awal,
            'tglakhir' => $akhir
        );
        return $array;
    }

    function get_filter_data_user() {
        $query = $this->db->query("SELECT id_group, nama_group  FROM user_group WHERE is_active='1'")->result_array();
        return $query;
    }

    function get_kabupaten() {
        $query = $this->db->query("SELECT id_kabupaten, nama_kabupaten  FROM kabupaten WHERE is_active='1'")->result_array();
        return $query;
    }

    function get_kecamatan() {
        $where = '';
        $id_kabupaten = $this->input->get('kabupaten');

        if (!empty($id_kabupaten)) {
            if ($id_kabupaten == 'all') {
                $where = '';
            } else {
                $where .= " AND id_kabupaten ='$id_kabupaten'";
            }
        }
        $query = $this->db->query("SELECT id_kecamatan, nama_kecamatan  FROM kecamatan WHERE is_active='1' $where")->result_array();
        return $query;
    }

    function get_all_data($jenis_return, $start = null, $limit = null) {
        if ($jenis_return == 'jumlah')
            $kolom = 'COUNT(lu.id_log) as jml';
        else {
            $kolom = " * ";
        }

        $id_kabupaten = $this->input->get('kabupaten');
        $id_kecamatan = $this->input->get('kecamatan');

        $tabel="FROM log_user lu
                        JOIN user u ON u.id_user=lu.id_user";
        if (!empty($id_kabupaten)) {
            $tabel = " FROM log_user lu
                        JOIN user u ON u.id_user=lu.id_user
                        JOIN kabupaten k ON k.id_kabupaten=lu.id_user WHERE k.id_user='$id_kabupaten' ORDER BY lu.activity_time DESC ";
        }
        if (!empty($id_kecamatan)) {
             $tabel = " FROM log_user lu
                        JOIN user u ON u.id_user=lu.id_user
                        JOIN kecamatan k ON k.id_kecamatan=lu.id_user WHERE k.id_user='$id_kecamatan' AND k.id_kabupaten='$id_kabupaten' ORDER BY lu.activity_time DESC "; 
        }
       

        if (!empty($this->input->get())) {
            $query = $this->db->query("SELECT $kolom $tabel");
        } else {
            if (isset($limit)) {
                $query = $this->db->query("SELECT $kolom $tabel LIMIT $start, $limit");
            } else {
                $query = $this->db->query("SELECT $kolom $tabel ");
            }
        }

        if ($jenis_return == 'jumlah')
            return $query->first_row()->jml;
        else {
            return $query->result_array();
        }
    }

    function whereHandler() {
        $where = '';
        $id_kabupaten = $this->input->get('kabupaten');
        $id_kecamatan = $this->input->get('kecamatan');
        $id_jenis_alsintan = $this->input->get('jenis');

        if (!empty($id_kabupaten)) {
            if ($id_kabupaten == 'all') {
                $where = '';
            } else {
                $where .= " AND al.id_kabupaten ='$id_kabupaten'";
            }
        }
        if (!empty($id_kecamatan)) {
            if ($id_kecamatan == 'all') {
                $where = '';
            } else {
                $where = $where . " AND m.id_brigade = '$id_kecamatan'";
            }
        }

        if (!empty($id_jenis_alsintan)) {
            if ($id_jenis_alsintan == 'all') {
                $where = '';
            } else {
                $where = $where . " AND al.id_jenis_alsintan= '$id_jenis_alsintan'";
            }
        }
        return $where;
    }

}
