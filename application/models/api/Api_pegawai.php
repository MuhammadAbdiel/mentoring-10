<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_pegawai extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function pegawai_master() {
        $get = $this->db->query("SELECT id_cabang, nama_cabang from cabang order by nama_cabang ASC");
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }else {
            $i = 0;
            foreach ($get->result() as $key => $r) {
                $result[$i]['id_cabang'] = $r->id_cabang;
                $result[$i]['nama_cabang'] = $r->nama_cabang;
                $i++;
            }

            $response['cabang'] = $result;

            $get_pegawai = $this->db->query("SELECT id_pegawai, nama, id_cabang from m_pegawai order by nama ASC");
            if ($get_pegawai->num_rows() == 0) {
                return ["status" => "failed", "message" => "Data tidak ditemukan."];
            }

            $j = 0;
            foreach ($get_pegawai->result() as $key => $k) {
                $pegawai[$j]['id_pegawai'] = $k->id_pegawai;
                $pegawai[$j]['nama'] = $k->nama;
                $pegawai[$j]['id_cabang'] = $k->id_cabang;
                $j++;
            }
            $response['pegawai_master'] = $pegawai;

            // serve
            return ["status" => "ok", "data" => $response];

        }

    }

    public function pegawai_byKantor($id_cabang)
    {
        $get_pegawai = $this->db->query("SELECT id_pegawai, nama, id_cabang from m_pegawai where id_cabang=?",array($id_cabang));
        if ($get_pegawai->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan.","data"=>null];
        }else {
            $j = 0;
            foreach ($get_pegawai->result() as $key => $r) {
                $pegawai[$j]['id_pegawai'] = $r->id_pegawai;
                $pegawai[$j]['nama'] = $r->nama;
                $pegawai[$j]['id_cabang'] = $r->id_cabang;
                $j++;
            }
            return ["status" => "ok","message"=>"Data ditemukan", "data" => $pegawai];

        }

    }



}
